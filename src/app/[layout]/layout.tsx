import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.scss";
import Header from "@/components/header/header";
import { NextIntlClientProvider, useMessages } from "next-intl";
import { ChakraProvider } from "@chakra-ui/react";
import ProgressBar from "../progressBar";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Nurobod TTB",
  description: "Generated by create next app",
};

interface RootLayoutProps {
  children: React.ReactNode;
  params: {
    locale: string;
  };
}

function RootLayout({
  children,
  params: { locale },
}: Readonly<RootLayoutProps>) {
  const message = useMessages();
  return (
    <html lang={locale}>
      <body className={inter.className}>
        <ProgressBar />
        <NextIntlClientProvider locale={locale} messages={message}>
          <ChakraProvider>
            <div className="">
              <Header />
              <div> {children}</div>
              {/* <div className="footer">footer</div> */}
            </div>
          </ChakraProvider>
        </NextIntlClientProvider>
      </body>
    </html>
  );
}

export default RootLayout;
