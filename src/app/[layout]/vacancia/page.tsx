import Footer from "@/components/Footer";
import Vacansia from "@/components/Vacancia";
import React from "react";

export default function Vacancia() {
  return (
    <div className="flex flex-col justify-between">
      <div><Vacansia /></div>
      <div className="">
        <Footer />
      </div>
    </div>
  );
}
