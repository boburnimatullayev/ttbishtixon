import About from "@/components/About";
import Contact from "@/components/Contact";
import Container from "@/components/Container";
import Footer from "@/components/Footer";
import Hero from "@/components/Hero/Hero";
import OurTeam from "@/components/OurTeam";
import Vacansia from "@/components/Vacancia";
import { useTranslations } from "next-intl";

function Home() {
  const t = useTranslations("IndexPage");

  return (
    <div>
      <Hero />
      <About />
      <OurTeam />
      <Contact />
      <Footer />
    </div>
  );
}

  export default Home;
