"use client";

import styles from "./hero.module.scss";
import React, { useRef } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Container from "../Container";
import bannerImg from "../../assets/10.jpg";
import Image from "next/image";
import { useTranslations } from "next-intl";

const settings = {
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
};

const Hero = () => {
  const t = useTranslations("Home");
  let sliderRef = useRef(null);
  return (
    <section>
      <div className={styles.sliderWrap}>
        <Slider
          ref={(slider: any) => {
            sliderRef = slider;
          }}
          {...settings}
          className={styles.slider}
        >
          <div className={styles.sliderItem}>
            <div className={styles.textWrap}>
              <h1>
                {t("title")} <br /> {t("title2")} <br />{t("title3")}
              </h1>
              <p>
                {t("deck")} 
             
              </p>
            </div>
            <Image className={styles.image} src={bannerImg} alt="bannerimg" />
          </div>
          <div className={styles.sliderItem}>
            <Image className={styles.image} src={bannerImg} alt="bannerimg" />
          </div>
        </Slider>
      </div>
      {/* </Container> */}
    </section>
  );
};

export default Hero;
