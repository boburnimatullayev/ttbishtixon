import {
  Box,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Heading,
  // Image,
  SimpleGrid,
  Text,
} from "@chakra-ui/react";
import React from "react";
import Container from "../Container";
import imaDoctor from "../../assets/doctorOne.png";
import Image from "next/image";

import styles from "./style.module.scss";
import { useTranslations } from "next-intl";

export default function OurTeam() {
  const t = useTranslations("Navigation")
  return (
    <section className={styles.ourTeamSection}>
      <Container>
        <Box  mb={"30px"}>
          <h2 id="ourTeam" className={styles.ourTitle}>{t("leadership")}</h2>
          <SimpleGrid
            spacing={4}
            templateColumns="repeat(auto-fill, minmax(300px, 1fr))"
          >
            <Card className={styles.card}>
              <CardHeader p={0}>
                <Image
                className={styles.img}
                  objectFit="cover"
                  height={300}
                  src={imaDoctor}
                  alt="Caffe Latte"
                />
              </CardHeader>
              <CardBody>
              <h3 className={styles.ourTeamName}>
              Nimatullayev Bobur

              </h3>
                <p>
                  View a summary of all your customers over the last month.
                </p>
              </CardBody>
              {/* <CardFooter>
                <Button className="outlineButton">Nimadir bo'ladi</Button>
              </CardFooter> */}
            </Card>
            <Card className={styles.card}>
              <CardHeader p={0}>
                <Image
                className={styles.img}
                  objectFit="cover"
                  height={300}
                  alt="Caffe Latte"
                  src={imaDoctor}
                />
              </CardHeader>
              <CardBody>
              <h3 className={styles.ourTeamName}>
              Nimatullayev Bobur

              </h3>
                <p>
                  View a summary of all your customers over the last month.
                </p>
              </CardBody>
              {/* <CardFooter>
                <Button className="outlineButton">Nimadir bo'ladi</Button>
              </CardFooter> */}
            </Card>
            <Card className={styles.card}>
              <CardHeader p={0}>
                <Image
                className={styles.img}
                  objectFit="cover"
                  height={300}
                  alt="Caffe Latte"
                  src={imaDoctor}
                />
              </CardHeader>
              <CardBody>
              <h3 className={styles.ourTeamName}>
              Nimatullayev Bobur

              </h3>
                <p>
                  View a summary of all your customers over the last month.
                </p>
              </CardBody>
              {/* <CardFooter>
                <Button className="outlineButton">Nimadir bo'ladi</Button>
              </CardFooter> */}
            </Card>
            <Card className={styles.card}>
              <CardHeader p={0}>
                <Image
                className={styles.img}
                  objectFit="cover"
                  height={300}
                  alt="Caffe Latte"
                  src={imaDoctor}
                />
              </CardHeader>
              <CardBody>
              <h3 className={styles.ourTeamName}>
              Nimatullayev Bobur

              </h3>
                <p>
                  View a summary of all your customers over the last month.
                </p>
              </CardBody>
              {/* <CardFooter>
                <Button className="outlineButton">Nimadir bo'ladi</Button>
              </CardFooter> */}
            </Card>
          </SimpleGrid>
        </Box>
      </Container>
    </section>
  );
}
