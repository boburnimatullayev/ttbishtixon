import React, { ReactNode } from "react";
import cls from "./styles.module.scss";

export default function Container({ children }: { children: ReactNode }) {
  return <div className={cls.container}>{children}</div>;
}
