import React from "react";
import Container from "../Container";
import {
  Box,
  Button,
  Center,
  Divider,
  HStack,
  ListItem,
  Text,
  UnorderedList,

} from "@chakra-ui/react";
import { FaFacebook, FaTelegram, FaYoutube } from "react-icons/fa";
import styles from './style.module.scss';
import Link from "next/link";
import { useLocale, useTranslations } from "next-intl";

export default function Footer() {
  const localActive = useLocale();
  const t = useTranslations("Navigation");
  return (
    <div className={styles.footer}>
      <Divider  mt={10} />
      <Container>
        <Box
        className={styles.footerBox}
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            marginTop: "30px",
            marginBottom: "10px",
          }}
        >
          <Text>Logo</Text>
          <UnorderedList className={styles.navLinkWrap} sx={{ display: "flex", gap: "35px" }}>
            <ListItem
              _active={{ transform: "translateY(1px)" }}
              sx={{ listStyle: "none", cursor: "pointer" }}
            >
             <Link className={styles.link} href={`/${localActive}/#about`}>
                {t("about")}
              </Link>
            </ListItem>
            <ListItem
              _active={{ transform: "translateY(1px)" }}
              sx={{ listStyle: "none", cursor: "pointer" }}
            >
              <Link className={styles.link} href={`/${localActive}/#ourTeam`}>
                {t("leadership")}
              </Link>
            </ListItem>
            <ListItem
              _active={{ transform: "translateY(1px)" }}
              sx={{ listStyle: "none", cursor: "pointer" }}
            >
               <Link className={styles.link} href={`/${localActive}/vacancia`}>
                {t("vakansiya")}
              </Link>
            </ListItem>
            <ListItem
              _active={{ transform: "translateY(1px)" }}
              sx={{ listStyle: "none", cursor: "pointer" }}
            >
               <Link className={styles.link} href={`/${localActive}/vacancia`}>
               {t("departments")}
              </Link>
            </ListItem>
            <ListItem
              _active={{ transform: "translateY(1px)" }}
              sx={{ listStyle: "none", cursor: "pointer" }}
            >
              <Link className={styles.link} href={`/${localActive}/#contact`}>
                {t("contact")}
              </Link>
            </ListItem>
          </UnorderedList>
          <HStack>
            <Center
              borderRadius={8}
              cursor={"pointer"}
              _hover={{ transition: "all 0.4s", background: "#e2e4f0" }}
              w="40px"
              h="40px"
              bg="#1877F2"
              color="white"
            >
              <FaFacebook />
            </Center>
            <Center
              borderRadius={8}
              cursor={"pointer"}
              _hover={{ transition: "all 0.4s", background: "#dfe0e6" }}
              w="40px"
              h="40px"
              bg="#0088cc"
              color="white"
            >
              <FaTelegram />
            </Center>
            <Center
              borderRadius={8}
              cursor={"pointer"}
              _hover={{ transition: "all 0.4s", background: "#e2e4f0" }}
              w="40px"
              h="40px"
              bg="#FF0000"
              color="white"
            >
              <FaYoutube />
            </Center>
          </HStack>
        </Box>
        <Box className={styles.protection} mb={5}>© 2024 Barcha huquqlar himoyalangan</Box>
      </Container>
    </div>
  );
}
