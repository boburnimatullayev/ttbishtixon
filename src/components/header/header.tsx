"use client";

import { useLocale, useTranslations } from "next-intl";
import Link from "next/link";
import LocalSwitcher from "./local-switcher";
import styles from "./header.module.scss";
import Container from "../Container";

import {
  Button,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  IconButton,
  useDisclosure,
} from "@chakra-ui/react";
import { BurgerIcon } from "@/assets/icon";
import { useRef } from "react";

export default function Header() {
  const localActive = useLocale();
  const t = useTranslations("Navigation");
  const { isOpen, onOpen, onClose } = useDisclosure();
  const btnRef = useRef();
  return (
    <header className={styles.header}>
      <Container>
        <nav className={styles.nav}>
          <div className={styles.navWrap}>
            <div className={styles.logo}>
              <img src="../../assets/logo.jpg" alt="logo" />
            </div>
            <div className={styles.linkWap}>
            {/* <Link className={styles.link} href="/">
                {t("home")}
              </Link> */}
              <Link className={styles.link} href={`/${localActive}/#about`}>
                {t("about")}
              </Link>
              <Link className={styles.link} href={`/${localActive}/#ourTeam`}>
                {t("leadership")}
              </Link>
              <Link className={styles.link} href={`/${localActive}/#contact`}>
                {t("contact")}
              </Link>
              <Link className={styles.link} href={`/${localActive}/vacancia`}>
                {t("vakansiya")}
              </Link>
              <Link className={styles.link} href={`/${localActive}/qvp`}>
                {t("departments")}
              </Link>
            </div>
          </div>
          <div className="flex gap-5">
         
            <LocalSwitcher  />
            <Button className="button"  >
              {t("contact")}
            </Button>
            <IconButton
              aria-label="bobur"
              className={styles.openIcon}
              border={"none"}
              icon={<BurgerIcon />}
              onClick={onOpen}
            />
          </div>
        </nav>
      </Container>
      <Drawer
        isOpen={isOpen}
        placement="right"
        onClose={onClose}
      >
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerBody>
          <DrawerHeader></DrawerHeader>
            <div className={styles.linkWapMobile}>
              {/* <Link className={styles.link} href="/">
                {t("home")}
              </Link> */}
              <Link onClick={onClose} className={styles.link} href={`/${localActive}/#about`}>
                {t("about")}
              </Link>
              <Link onClick={onClose} className={styles.link} href={`/${localActive}/#ourTeam`}>
                {t("leadership")}
              </Link>
              <Link onClick={onClose} className={styles.link} href={`/${localActive}/#contact`}>
                {t("contact")}
              </Link>
              <Link onClick={onClose} className={styles.link} href={`${localActive}/vacancia`}>
                {t("vakansiya")}
              </Link>
              <Link onClick={onClose} className={styles.link} href={`/${localActive}/qvp`}>
                {t("departments")}
              </Link>
            </div>
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </header>
  );
}
