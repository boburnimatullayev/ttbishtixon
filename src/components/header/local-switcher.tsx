"use client";

import {
  Button,
  Menu,
  MenuButton,
  MenuItem,
  MenuItemOption,
  MenuList,
  MenuOptionGroup,
} from "@chakra-ui/react";
import { useLocale } from "next-intl";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { ChangeEvent, useTransition } from "react";

export default function LocalSwitcher() {
  const [isPending, startTransition] = useTransition();
  const router = useRouter();
  const path = usePathname()
  const localActive = useLocale();
  console.log("event", path);

  const onSelectChange = (lngProps: any) => {
    console.log(lngProps);
    startTransition(() => {
      path.includes("/qvp") ? 
      router.push(`/${lngProps}/qvp`) : router.push(`/${lngProps}${path.slice(3)}`);
    });
  };
  return (
    <>
      <Menu closeOnSelect={false} >
        <MenuButton background={"#E0ECE9"} color={"#33836E"} as={Button}>{localActive === "uz" ? "Uz" : "Ru"}</MenuButton>
        <MenuList minWidth='140px'>
          <MenuOptionGroup onChange={(e) => onSelectChange(e)} defaultValue={localActive} type="radio">
            <MenuItemOption value="uz">Uz</MenuItemOption>
            <MenuItemOption value="ru">Ru</MenuItemOption>
          </MenuOptionGroup>
        </MenuList>
      </Menu>
    </>
  );
}
