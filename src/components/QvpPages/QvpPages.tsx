import React from "react";
import styles from "./styles.module.scss";
import Container from "../Container";
import Footer from "../Footer";
import { Card } from "@chakra-ui/react";
import { data } from "./data";

const QvpPages = () => {
 
  return (
    <section className={styles.qvpSection}>
      <Container>
        <div className={styles.qvpWrap}>
          <h1 className={styles.allQvpTitle}>
            Shahar bo`yicha Qvp punkitlar ro`yxati
          </h1>

          <div className={styles.table}>
            {data.map((item) => (
              <>
                <div className={styles.card}>
                  <p className={styles.address}>{item.address}</p>
                  <p className={styles.doctor}>{item.doctor}</p>

                  <a className={styles.link} href={`tel:${item.tel}`}>
                    {item.tel}
                  </a>
                </div>
              </>
            ))}
          </div>
        </div>
      </Container>
      <Footer />
    </section>
  );
};

export default QvpPages;
