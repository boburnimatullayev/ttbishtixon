export const data = [
  {
    id: 1,
    address: "Nurobod tuman koʻp tarmoqli markazi poliklinikasi",
    doctor: "Musaxonov saidmurod",
    tel: "+998979123030",
  },
  {
    id: 2,
    address: "Nurobod tumani Xalqobod oilaviy poliklinikasi",
    doctor: "Babanov O'tkir Ubaydullaevich",
    tel: "+998933587470",
  },
  {
    id: 3,
    address: "Nurobod tumani O'zbekiston oilaviy poliklinika",
    doctor: "Xujamuradov Tirkash Ro'zievich",
    tel: "+998933556060",
  },
  {
    id: 4,
    address: "Nurobod tumani Mustaqillik oilaviy poliklinika",
    doctor: "Tuymanov Tulqin Shukirovich",
    tel: "+998976125865",
  },
  {
    id: 5,
    address: "Nurobod tumani Nurli diyor oilaviy poliklinikasi",
    doctor: "Hakimova Nasiba Jurakulovna",
    tel: "+998994296789",
  },
  {
    id: 6,
    address: "Nurobod tumani Mitan poliklinikasi",
    doctor: "Ramanqulov Zafar Nurmaxamadovich",
    tel: "+998996778579",
  },
  {
    id: 7,
    address: "Nurobod tumani Mitan poliklinikasi",
    doctor: "Ramanqulov Zafar Nurmaxamadovich",
    tel: "+998996778579",
  },
  {
    id: 8,
    address: "Nurobod tumani Mitan poliklinikasi",
    doctor: "Ramanqulov Zafar Nurmaxamadovich",
    tel: "+998996778579",
  },
  {
    id: 9,
    address: "Nurobod tumani Mitan poliklinikasi",
    doctor: "Ramanqulov Zafar Nurmaxamadovich",
    tel: "+998996778579",
  },

];
