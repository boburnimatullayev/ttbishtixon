import React from "react";
import Container from "../Container";
import {
  Box,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Heading,
  Stack,
  Text,
} from "@chakra-ui/react";
import { FaLocationArrow } from "react-icons/fa";
import { CiClock2, CiLocationOn } from "react-icons/ci";

export default function Vacansia() {
  return (
    <Container>
      <Text
        sx={{
          fontSize: "30px",
          fontWeight: "bold",
          textAlign: "center",
          marginBottom: "15px",
          marginTop: "100px",
          color: "black !important",
        }}
      >
        Bo`sh ish o`rinlari
      </Text>
      <Box>
        <Stack
          spacing="4"
          sx={{
            display: "grid",
            gridTemplateColumns: "repeat(auto-fill, minmax(345px, 1fr))",
          }}
        >
          <Card variant={"outline"}>
            <CardHeader>
              <Heading color={"#6942c6"} size="sm">
                Iqtisodchi
              </Heading>
            </CardHeader>
            <CardBody pt={0}>
              <Text>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Eligendi non quis exercitationem culpa nesciunt nihil aut
                nostrum explicabo reprehenderit optio amet ab temporibus
                asperiores quasi cupiditate. Voluptatum ducimus voluptates
                voluptas?
              </Text>
            </CardBody>
            <CardFooter pt={0} className="flex items-center gap-10">
              <Box className="flex gap-2">
                <CiLocationOn size={22} />
                Ofis
              </Box>
              <Box className="flex gap-2">
                <CiClock2 size={22} />
                To`liq ish kuni
              </Box>
            </CardFooter>
          </Card>
          <Card variant={"outline"}>
            <CardHeader>
              <Heading color={"#6942c6"} size="sm">
                Omborchi
              </Heading>
            </CardHeader>
            <CardBody pt={0}>
              <Text>
                {" "}
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Eligendi non quis exercitationem culpa nesciunt nihil aut
                nostrum{" "}
              </Text>
            </CardBody>
            <CardFooter pt={0} className="flex items-center gap-10">
              <Box className="flex gap-2">
                <CiLocationOn size={22} />
                Ofis
              </Box>
              <Box className="flex gap-2">
                <CiClock2 size={22} />
                To`liq ish kuni
              </Box>
            </CardFooter>
          </Card>

          <Card variant={"outline"}>
            <CardHeader>
              <Heading color={"#6942c6"} size="sm">
                Buhgalter
              </Heading>
            </CardHeader>
            <CardBody pt={0}>
              <Text>
                {" "}
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Eligendi non quis exercitationem culpa nesciunt nihil aut
                nostrum explicabo reprehenderit optio amet ab temporibus
                asperiores quasi cupiditate.
              </Text>
            </CardBody>
            <CardFooter pt={0} className="flex items-center gap-10">
              <Box className="flex gap-2">
                <CiLocationOn size={22} />
                Ofis
              </Box>
              <Box className="flex gap-2">
                <CiClock2 size={22} />
                To`liq ish kuni
              </Box>
            </CardFooter>
          </Card>
          <Card variant={"outline"}>
            <CardHeader>
              <Heading color={"#6942c6"} size="sm">
                Shafyor
              </Heading>
            </CardHeader>
            <CardBody pt={0}>
              <Text>
                {" "}
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Eligendi non quis exercitationem culpa nesciunt nihil aut
                nostrum explicabo reprehenderit optio amet ab temporibus
                asperiores quasi cupiditate. Voluptatum ducimus voluptates
                voluptas?
              </Text>
            </CardBody>
            <CardFooter pt={0} className="flex items-center gap-10">
              <Box className="flex gap-2">
                <CiLocationOn size={22} />
                Ofis
              </Box>
              <Box className="flex gap-2">
                <CiClock2 size={22} />
                To`liq ish kuni
              </Box>
            </CardFooter>
          </Card>
        </Stack>
      </Box>
    </Container>
  );
}
