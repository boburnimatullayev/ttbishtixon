import React from "react";
import Container from "../Container";
import { Box } from "@chakra-ui/react";
import styles from "./style.module.scss";
import imgDoctor from "../../assets/doktorteam.jpg"
import Image from "next/image";
import { useTranslations } from "next-intl";

export default function About() {
  const t = useTranslations("Navigation");

  return (
    <Container>
      <Box className={styles.aboutBox}>
        <h2  id="about"  className={styles.aboutTitle}>{t("about")}</h2>

        <div className={styles.contend}>
          <div className={styles.leftSide}>
            <h3 className={styles.leftSideTitle}>{t("exactly")}</h3>
            <p className={styles.deck}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry`s standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make Lorem Ipsum is simply dummy
                text of the printing and typesetting industry. Lorem Ipsum has
                been the industry`s standard dummy text ever since the 1500s,
         
            </p>
          </div>
          <div className={styles.rightSide}>
            <Image className={styles.img}  src={imgDoctor} alt="doktor img" />
          </div>
        </div>
      </Box>
    </Container>
  );
}
