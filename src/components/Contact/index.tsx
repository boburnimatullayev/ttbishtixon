"use client";
import { PhoneIcon } from "@chakra-ui/icons";
import Container from "../Container";
import {
  Box,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputLeftElement,
  Text,
  Textarea,
} from "@chakra-ui/react";
import styles from "./style.module.scss";
import { useTranslations } from "next-intl";

export default function Contact() {
  const t = useTranslations("Navigation")
  return (
    <Container>
      <Text id="contact"     className={styles.contactTitle}>{t("contact")}</Text>
      <Box className={styles.contactWrap}>
        <div className={styles.mapWrap}>
          <iframe
            className={styles.map}
            src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d691.856518228035!2d66.48498517295393!3d39.96521746305223!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1714063187530!5m2!1sen!2s"
         
            loading="lazy"
      
          ></iframe>
        </div>
        <div className={styles.contact}>
          <FormControl>
            <FormLabel className={styles.label} mb={1}>
              Ismingizni kiriting
            </FormLabel>
            <Input
              className={styles.input}
              type="text"
              placeholder="Ismingizni kiriting"
            />
            <FormLabel className={styles.label} mb={1} mt={3}>
              Raqamingizni kiriting
            </FormLabel>
            <InputGroup flexDirection={"column"}>
              <InputLeftElement pointerEvents="none">
                <PhoneIcon color="gray.300" />
              </InputLeftElement>
              <Input
                className={styles.input}
                type="tel"
                placeholder="Phone number"
              />
            </InputGroup>
            <FormLabel className={styles.label} mb={1} mt={3}>
              Xabaringizni kiriting
            </FormLabel>
            <Textarea
              className={styles.deck}
              placeholder="Xabaringizni kiriting..."
              height={200}
            />
            <div className={styles.buttonWrap}>
              <button className={styles.button}>yuborish</button>
            </div>
          </FormControl>
        </div>
      </Box>
    </Container>
  );
}
